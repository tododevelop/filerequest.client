﻿using FileRequest.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace FileRequest.Client.Service
{
    public class RequestManager
    {
        public async System.Threading.Tasks.Task<RequestState> GetFilesAsync(string baseUrl, string[] fileUrls)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                var pairs = new Dictionary<string, string>();
                var i = 0;
                foreach(var url in fileUrls)
                {
                    pairs.Add("url[" + i + "]", url);
                    i++;
                }
                using (var formContent = new FormUrlEncodedContent(pairs))
                {
                    var response = await client.PostAsync("getfiles", formContent).ConfigureAwait(true);
                    if (!response.IsSuccessStatusCode) return null;
                    var r = response.Content.ReadAsStringAsync().Result;
                    RequestState result = JsonConvert.DeserializeObject<RequestState>(r);
                    //RequestState result = JsonConvert.DeserializeObject<RequestState>(response.Content.ReadAsStringAsync().Result);

                    //if (result.IsOk) result.Result = JsonConvert.DeserializeObject<List<FileDto>>(result.Result.ToString());
                    return result;
                }
            }
        }

        public async System.Threading.Tasks.Task<RequestStateUrl> GetFilesUrlAsync(string baseUrl, string[] fileUrls)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                var pairs = new Dictionary<string, string>();
                var i = 0;
                foreach (var url in fileUrls)
                {
                    pairs.Add("url[" + i + "]", url);
                    i++;
                }
                using (var formContent = new FormUrlEncodedContent(pairs))
                {
                    var response = await client.PostAsync("getfilewithurl", formContent).ConfigureAwait(true);
                    if (!response.IsSuccessStatusCode) return null;
                    var r = response.Content.ReadAsStringAsync().Result;
                    RequestStateUrl result = JsonConvert.DeserializeObject<RequestStateUrl>(r);
                    //RequestState result = JsonConvert.DeserializeObject<RequestState>(response.Content.ReadAsStringAsync().Result);

                    //if (result.IsOk) result.Result = JsonConvert.DeserializeObject<List<FileDto>>(result.Result.ToString());
                    return result;
                }
            }
        }

        internal async Task GetFileExcelAsync(string baseUrl)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                await client.GetAsync("getfileexcel").ConfigureAwait(true);
            }
        }
    }
}
