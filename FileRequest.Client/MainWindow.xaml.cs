﻿using FileRequest.Client.Service;
using FileRequest.Core;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;

namespace FileRequest.Client
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button1_ClickAsync(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(async () =>
            {
                var manager = new RequestManager();
                string[] urls = new string[]
                {
                    "http://bass32.ru/MainPage/GetImage/6",
//                    "http://bass32.ru/MainPage/GetImage/6",
                    "http://testfileload-ru.1gb.ru/Files/videoplayback.mp4",
//                    "https://instagram.fods1-1.fna.fbcdn.net/v/t51.2885-15/e35/s1080x1080/74687639_437111640536982_4965583491797710659_n.jpg?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=106&oh=639b2cae902f0965caf940f671cbfd4f&oe=5E93C9BF",
                    "https://bass32.ru/MainPage/GetImage/1",
                    "https://bass32.ru/MainPage/GetImage/7",
                    "https://bass32.ru/MainPage/GetImage/5",
                    //"https://instagram.fods1-1.fna.fbcdn.net/v/t50.2886-16/55999624_1063394053847893_2857969339605539963_n.mp4?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=zAbpv8MlMLUAX-am815&oe=5E1CF234&oh=2c3706b9ff2e59cde4d919c1c9323f05"
                    //"https://instagram.fods1-1.fna.fbcdn.net/v/t50.2886-16/75601471_474098653449136_3626238157429620463_n.mp4?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=WgSsdnpQxO4AX-VRxMi&oe=5DFE908A&oh=992301be08f365587a3e97cafcd8e203"
                    "http://testfileload-ru.1gb.ru/Files/videoplayback.mp4"
                };

                var result = await manager.GetFilesAsync("https://localhost:44363/ ", urls).ConfigureAwait(true);
                //var result = await manager.GetFilesAsync("http://testfileload-ru.1gb.ru/ ", urls).ConfigureAwait(true);

                if (result.IsOk)
                {
                    if (result.Result is List<FileDto> files)
                    {
                        FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

                        DialogResult res = folderBrowser.ShowDialog();

                        if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
                        {
                            var i = 0;
                            foreach (var file in files)
                            {
                                string path;
                                if (file.MimeType == "video/mp4") path = folderBrowser.SelectedPath + @"\" + file.Name + @".mp4";
                                else path = folderBrowser.SelectedPath + @"\" + file.Name + @".jpg";
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    stream.Write(file.Body, 0, file.Body.Length);
                                }
                                i++;
                            }
                            System.Windows.MessageBox.Show("Готово", "Информация", MessageBoxButton.OK);
                        }
                        else System.Windows.MessageBox.Show("Сохранение файлов отменено..", "Информация", MessageBoxButton.OK);
                    }
                }
                else
                {
                    if (result.IsOk == false)
                    {
                        System.Windows.MessageBox.Show(result.ErrorMessage, "Ошибка!", MessageBoxButton.OK);
                    }
                }
            });
        }

        private void Button2_ClickAsync(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(async () =>
            {
                var manager = new RequestManager();
                string[] urls = new string[]
                {
                    "http://bass32.ru/MainPage/GetImage/6",
                    //"https://instagram.fods1-1.fna.fbcdn.net/v/t51.2885-15/e35/s1080x1080/74687639_437111640536982_4965583491797710659_n.jpg?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=106&oh=639b2cae902f0965caf940f671cbfd4f&oe=5E93C9BF",
                    "http://bass32.ru/MainPage/GetImage/6",
                    "http://bass32.ru/MainPage/GetImage/1",
                    "http://bass32.ru/MainPage/GetImage/7",
                    "http://bass32.ru/MainPage/GetImage/5",
                    //"https://instagram.fods1-1.fna.fbcdn.net/v/t50.2886-16/55999624_1063394053847893_2857969339605539963_n.mp4?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=zAbpv8MlMLUAX-am815&oe=5E1CF234&oh=2c3706b9ff2e59cde4d919c1c9323f05",
                    //"https://instagram.fods1-1.fna.fbcdn.net/v/t50.2886-16/75601471_474098653449136_3626238157429620463_n.mp4?_nc_ht=instagram.fods1-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=WgSsdnpQxO4AX-VRxMi&oe=5DFE908A&oh=992301be08f365587a3e97cafcd8e203"
                    "http://testfileload-ru.1gb.ru/Files/videoplayback.mp4"
                };

                //var result = await manager.GetFilesUrlAsync("https://localhost:44363/ ", urls).ConfigureAwait(true);
                var result = await manager.GetFilesUrlAsync("http://testfileload-ru.1gb.ru/ ", urls).ConfigureAwait(true);

                if (result.IsOk)
                {
                    if (result.Result is List<FileUrlDto> files)
                    {
                        FolderBrowserDialog folderBrowser = new FolderBrowserDialog();

                        DialogResult res = folderBrowser.ShowDialog();

                        if (!string.IsNullOrWhiteSpace(folderBrowser.SelectedPath))
                        {
                            var i = 0;
                            foreach (var file in files)
                            {
                                string path;
                                path = folderBrowser.SelectedPath + @"\" + file.Name;
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    stream.Write(file.Body, 0, file.Body.Length);
                                }
                                i++;
                            }
                            System.Windows.MessageBox.Show("Готово", "Информация", MessageBoxButton.OK);
                        }
                        else System.Windows.MessageBox.Show("Сохранение файлов отменено..", "Информация", MessageBoxButton.OK);
                    }
                }
                else
                {
                    if (result.IsOk == false)
                    {
                        System.Windows.MessageBox.Show(result.ErrorMessage, "Ошибка!", MessageBoxButton.OK);
                    }
                }
            });
        }

        private void Button3_ClickAsync(object sender, RoutedEventArgs e)
        {
            var manager = new RequestManager();

            Dispatcher.Invoke(async () =>
            {
                await manager.GetFileExcelAsync("https://localhost:44363/ ").ConfigureAwait(true);
                //await manager.GetFileExcelAsync("http://testfileload-ru.1gb.ru/ ").ConfigureAwait(true);
            });
        }
    }
}